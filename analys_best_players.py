import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Načítání dat
file_path_cleaned = './cleaned_data.csv'
file_path_mvp = './mvp_data.csv'

data_cleaned = pd.read_csv(file_path_cleaned)
data_mvp = pd.read_csv(file_path_mvp)

# Sloučení dat na základě jména hráče a roku
data_combined = pd.merge(data_cleaned, data_mvp, on=['Player', 'Year'], how='inner', suffixes=('_all', '_mvp'))

# Box Plot pro body (PTS) nejlepších hráčů podle let
plt.figure(figsize=(12, 8))
sns.boxplot(x='Year', y='PTS_all', data=data_combined)
plt.title('Rozdělení bodů nejlepších hráčů podle let')
plt.xlabel('Rok')
plt.ylabel('Body')
plt.xticks(rotation=45)
plt.show()

# Scatter Plot pro body (PTS) vs. minuty na hřišti (MP) pro nejlepší hráče
plt.figure(figsize=(10, 6))
sns.scatterplot(x='MP_all', y='PTS_all', data=data_combined)
plt.title('Body vs. Minuty na hřišti pro nejlepší hráče')
plt.xlabel('Minuty na hřišti')
plt.ylabel('Body')
plt.show()

# Scatter Plot pro body (PTS) vs. věk (Age) pro nejlepší hráče
plt.figure(figsize=(10, 6))
sns.scatterplot(x='Age_all', y='PTS_all', data=data_combined)
plt.title('Body vs. Věk pro nejlepší hráče')
plt.xlabel('Věk')
plt.ylabel('Body')
plt.show()

# Pair Plot pro hlavní statistické ukazatele nejlepších hráčů
sns.pairplot(data_combined[['PTS_all', 'MP_all', 'Age_all', 'TRB_all', 'AST_all']])
plt.show()

# Violin Plot pro rozdělení bodů (PTS) nejlepších hráčů podle týmů
plt.figure(figsize=(12, 8))
sns.violinplot(x='Tm_all', y='PTS_all', data=data_combined)
plt.title('Rozdělení bodů nejlepších hráčů podle týmů')
plt.xlabel('Tým')
plt.ylabel('Body')
plt.xticks(rotation=90)
plt.show()
