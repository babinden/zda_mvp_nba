import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Načítání dat
file_path = './cleaned_data.csv'  # Nahraďte skutečnou cestou k souboru
data = pd.read_csv(file_path)

# Box Plot pro body podle pozic
plt.figure(figsize=(12, 8))
sns.boxplot(x='Pos', y='PTS', data=data)
plt.title('Rozdělení bodů podle pozic')
plt.xlabel('Pozice')
plt.ylabel('Body')
plt.show()

# Box Plot pro body podle let
plt.figure(figsize=(12, 8))
sns.boxplot(x='Year', y='PTS', data=data)
plt.title('Rozdělení bodů podle let')
plt.xlabel('Rok')
plt.ylabel('Body')
plt.xticks(rotation=45)
plt.show()

# Scatter Plot pro body vs. minuty na hřišti
plt.figure(figsize=(10, 6))
sns.scatterplot(x='MP', y='PTS', data=data)
plt.title('Body vs. Minuty na hřišti')
plt.xlabel('Minuty na hřišti')
plt.ylabel('Body')
plt.show()

# Scatter Plot pro body vs. věk
plt.figure(figsize=(10, 6))
sns.scatterplot(x='Age', y='PTS', data=data)
plt.title('Body vs. Věk')
plt.xlabel('Věk')
plt.ylabel('Body')
plt.show()

# Pair Plot pro hlavní statistické ukazatele
sns.pairplot(data[['PTS', 'MP', 'Age', 'TRB', 'AST']])
plt.show()

# Violin Plot pro rozdělení bodů podle týmů
plt.figure(figsize=(12, 8))
sns.violinplot(x='Tm', y='PTS', data=data)
plt.title('Rozdělení bodů podle týmů')
plt.xlabel('Tým')
plt.ylabel('Body')
plt.xticks(rotation=90)
plt.show()
