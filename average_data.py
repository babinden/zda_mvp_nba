import pandas as pd
import matplotlib.pyplot as plt

# Načítání dat
file_path_cleaned = './cleaned_data.csv'
file_path_mvp = './mvp_data.csv'

data_cleaned = pd.read_csv(file_path_cleaned)
data_mvp = pd.read_csv(file_path_mvp)

# Sloučení dat na základě jména hráče a roku
data_combined = pd.merge(data_cleaned, data_mvp, on=['Player', 'Year'], how='inner', suffixes=('_all', '_mvp'))

# Výpočet průměrných hodnot pro každý ukazatel
mean_stats = data_combined[['PTS_all', 'TRB_all', 'AST_all', 'MP_all', 'Age_all']].mean()

# Vytvoření DataFrame pro průměrné hodnoty
mean_stats_df = mean_stats.reset_index()
mean_stats_df.columns = ['Ukazatel', 'Průměrná hodnota']

# Vytvoření sloupcového grafu
plt.figure(figsize=(10, 6))
plt.bar(mean_stats_df['Ukazatel'], mean_stats_df['Průměrná hodnota'], color='skyblue')
plt.title('Průměrné hodnoty hlavních ukazatelů nejlepších hráčů')
plt.xlabel('Ukazatel')
plt.ylabel('Průměrná hodnota')
plt.show()
