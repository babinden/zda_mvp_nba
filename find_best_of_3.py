import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler

def analyze_best_players():
    file_path_cleaned = './cleaned_data.csv'
    file_path_mvp = './mvp_data.csv'
    file_path_candidates = './mvp_candidates_2022.csv'

    data_cleaned = pd.read_csv(file_path_cleaned)
    data_mvp = pd.read_csv(file_path_mvp)
    data_candidates = pd.read_csv(file_path_candidates)

    # Sloučení dat na základě jména hráče a roku
    data_combined = pd.merge(data_cleaned, data_mvp, on=['Player', 'Year'], how='inner', suffixes=('_all', '_mvp'))

    # Přidání sloupce 'MVP', kde všichni držitelé titulu MVP mají hodnotu 1
    data_combined['MVP'] = 1

    # Přidání dat s hodnotou 0 pro vyvážení tříd
    # Vytvoření dat se stejnými charakteristikami, ale s hodnotou 0
    non_mvp_data = data_combined.copy()
    non_mvp_data['MVP'] = 0

    # Sloučení dat
    all_data = pd.concat([data_combined, non_mvp_data], ignore_index=True)

    # Příprava dat pro model
    features_all = ['PTS_all', 'TRB_all', 'AST_all', 'MP_all', 'Age_all']
    X = all_data[features_all]
    y = all_data['MVP']

    # Škálování dat
    scaler = StandardScaler()
    X_scaled = scaler.fit_transform(X)

    # Rozdělení dat na trénovací a testovací sady
    X_train, X_test, y_train, y_test = train_test_split(X_scaled, y, test_size=0.2, random_state=42)

    # Trénování modelu
    model = RandomForestClassifier(random_state=42)
    model.fit(X_train, y_train)

    # Přejmenování sloupců v datech kandidátů
    data_candidates.columns = ['Player', 'Year', 'PTS_all', 'TRB_all', 'AST_all', 'MP_all', 'Age_all']

    # Příprava dat kandidátů
    X_candidates = data_candidates[features_all]
    X_candidates_scaled = scaler.transform(X_candidates)

    # Predikce pravděpodobnosti získání ocenění MVP
    candidates_prob = model.predict_proba(X_candidates_scaled)[:, 1]

    # Přidání pravděpodobností do DataFrame
    data_candidates['MVP_Probability'] = candidates_prob

    # Třídění kandidátů podle pravděpodobnosti a výběr top 3
    top_candidates = data_candidates.sort_values(by='MVP_Probability', ascending=False).head(3)

    return top_candidates

if __name__ == '__main__':
    top_candidates = analyze_best_players()
    print(top_candidates)
