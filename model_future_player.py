import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

# Krok 1: Analýza nejlepších hráčů (kód z analys_best_players.py)
def analyze_best_players():
    file_path_cleaned = './cleaned_data.csv'  # Uveďte skutečnou cestu k souboru
    file_path_mvp = './mvp_data.csv'  # Uveďte skutečnou cestu k souboru
    
    data_cleaned = pd.read_csv(file_path_cleaned)
    data_mvp = pd.read_csv(file_path_mvp)
    
    data_combined = pd.merge(data_cleaned, data_mvp, on=['Player', 'Year'], how='inner', suffixes=('_all', '_mvp'))

    # Box Plot pro body (PTS) nejlepších hráčů podle let
    plt.figure(figsize=(12, 8))
    sns.boxplot(x='Year', y='PTS_all', data=data_combined)
    plt.title('Rozdělení bodů nejlepších hráčů podle roků')
    plt.xlabel('Rok')
    plt.ylabel('Body')
    plt.xticks(rotation=45)
    plt.show()

    # Scatter Plot pro body (PTS) vs. minuty na hřišti (MP) pro nejlepší hráče
    plt.figure(figsize=(10, 6))
    sns.scatterplot(x='MP_all', y='PTS_all', data=data_combined)
    plt.title('Body vs. Minuty na hřišti pro nejlepší hráče')
    plt.xlabel('Minuty na hřišti')
    plt.ylabel('Body')
    plt.show()

    # Scatter Plot pro body (PTS) vs. věk (Age) pro nejlepší hráče
    plt.figure(figsize=(10, 6))
    sns.scatterplot(x='Age_all', y='PTS_all', data=data_combined)
    plt.title('Body vs. Věk pro nejlepší hráče')
    plt.xlabel('Věk')
    plt.ylabel('Body')
    plt.show()

    # Pair Plot pro hlavní statistické ukazatele nejlepších hráčů
    sns.pairplot(data_combined[['PTS_all', 'MP_all', 'Age_all', 'TRB_all', 'AST_all']])
    plt.show()

    # Violin Plot pro rozdělení bodů (PTS) nejlepších hráčů podle týmů
    plt.figure(figsize=(12, 8))
    sns.violinplot(x='Tm_all', y='PTS_all', data=data_combined)
    plt.title('Rozdělení bodů nejlepších hráčů podle týmů')
    plt.xlabel('Tým')
    plt.ylabel('Body')
    plt.xticks(rotation=90)
    plt.show()
    
    return data_combined

# Krok 2: Vytvoření modelu pro predikci budoucího MVP (kód z model_future_player.py)
def predict_future_mvp(data_combined):
    mvp_stats = data_combined[['PTS_all', 'TRB_all', 'AST_all', 'MP_all', 'Age_all']]
    mean_stats = mvp_stats.mean()

    # Vytvoření sloupce 'MVP', kde všichni držitelé titulu MVP mají hodnotu 1, ostatní 0
    data_combined['MVP'] = 1

    # Příprava dat pro model
    features = ['PTS_all', 'TRB_all', 'AST_all', 'MP_all', 'Age_all']
    X = data_combined[features]
    y = data_combined['MVP']

    # Rozdělení dat na trénovací a testovací sady
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    # Trénování modelu
    model = RandomForestClassifier(random_state=42)
    model.fit(X_train, y_train)

    # Predikce a vyhodnocení modelu
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    print(f'Přesnost modelu: {accuracy:.2f}')

    # Použití modelu pro predikci budoucího MVP
    next_year_candidate = pd.DataFrame([mean_stats])
    predicted_mvp = model.predict(next_year_candidate)
    predicted_mvp_proba = model.predict_proba(next_year_candidate)

    print(f'Předpokládaný MVP: {predicted_mvp}')
    print(f'Pravděpodobnost: {predicted_mvp_proba}')

# Hlavní funkce pro spuštění obou kroků
def main():
    data_combined = analyze_best_players()
    predict_future_mvp(data_combined)

if __name__ == '__main__':
    main()
