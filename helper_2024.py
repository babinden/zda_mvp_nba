import pandas as pd

player_names_2024 = [
    'Nikola Jokic', 'Joel Embiid', 'Giannis Antetokounmpo', 'Luka Doncic',
    'Jayson Tatum', 'Stephen Curry', 'Kevin Durant', 'LeBron James',
    'Anthony Davis', 'Ja Morant'
]

mean_stats_2024 = {
    'PTS': [26.4, 30.0, 29.5, 28.0, 26.5, 27.8, 26.0, 25.5, 24.5, 26.0],
    'TRB': [12.4, 11.7, 12.0, 9.0, 8.2, 5.5, 7.1, 8.0, 11.0, 6.5],
    'AST': [9.0, 4.2, 5.5, 8.5, 4.1, 6.3, 5.0, 7.5, 3.5, 7.8],
    'MP': [35.0, 34.5, 33.0, 36.0, 34.0, 34.8, 33.5, 34.0, 32.5, 34.0],
    'Age': [29, 30, 29, 25, 26, 36, 34, 39, 31, 24]
}

candidates_2024_df = pd.DataFrame(mean_stats_2024)
candidates_2024_df.insert(0, 'Player', player_names_2024)
candidates_2024_df.insert(1, 'Year', 2024)

# save CSV
file_path_candidates_2024 = 'mvp_candidates_2024.csv'
candidates_2024_df.to_csv(file_path_candidates_2024, index=False)

candidates_2024_df
